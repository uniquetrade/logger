<?php
namespace UTR\Logger;

class SearchLog
{
    const STATUS_FOUND = 'found';
    const STATUS_FOUND_AVAILABLE_IN_OTHER_CITY = 'found_available_in_other_city';
    const STATUS_FOUND_NOT_AVAILABLE = 'found_not_available';
    const STATUS_NOT_FOUND = 'not_found';

    /**
     * @var  string
     */
    private $client;

    /**
     * @var  \DateTime
     */
    private $date;

    /**
     * @var  string
     */
    private $article;

    /**
     * @var  string
     */
    private $brand;

    /**
     * @var  string
     */
    private $status;

    /**
     * @var string
     */
    private $articleInside;

    public function __construct($logger, $data)
    {
        $this->client = isset($data['agentExternalId']) ? $data['agentExternalId'] : null;
        $this->date = new \DateTime();
        $this->article = isset($data['keyword']) ? $data['keyword'] : null;

        switch ($logger) {
            case Logger::SEARCH_OEM_NOT_FOUND_LOG:
                $this->brand = 'noname';
                $this->status = self::STATUS_NOT_FOUND;
                break;
            case Logger::SEARCH_OEM_LOG:
                if(count($data['searchResults']) == 1) {
                    $result = reset($data['searchResults']);
                    if (isset($result['original']) && isset($result['original'][0])) {
                        $this->brand = $result['original'][0]->brand;
                    }

                    $remainOnMainStorage = $remain = 0;
                    foreach($result['original'] as $item) {
                        if($item->is_main_storage) {
                            $remainOnMainStorage =  $item->remain;
                        }
                        $remain += $item->remain;
                    }

                    if ($remain == 0) {
                        $this->status = self::STATUS_FOUND_NOT_AVAILABLE;
                    } elseif($remainOnMainStorage == 0) {
                        $this->status = self::STATUS_FOUND_AVAILABLE_IN_OTHER_CITY;
                    } else {
                        $this->status = self::STATUS_FOUND;
                    }

                    $this->articleInside = $item->article_inside;
                }
                break;
        }
    }


    /**
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param string $client
     * @return SearchLog
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return SearchLog
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param string $article
     * @return SearchLog
     */
    public function setArticle($article)
    {
        $this->article = $article;
        return $this;
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     * @return SearchLog
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return SearchLog
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getArticleInside()
    {
        return $this->articleInside;
    }

    /**
     * @param string $articleInside
     */
    public function setArticleInside($articleInside)
    {
        $this->articleInside = $articleInside;
    }
}