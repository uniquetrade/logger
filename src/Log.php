<?php

namespace UTR\Logger;

class Log
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $data;


    /**
     * Log constructor.
     * @param string $username
     * @param string $action
     * @param array $data
     */
    public function __construct($username, $action, $data)
    {
        $this->date = new \DateTime();
        $this->username = $username;
        $this->action = $action;
        $this->data = serialize($data);
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Log
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return Log
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return Log
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return unserialize($this->data);
    }

    /**
     * @return string
     */
    public function getSerializedData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return Log
     */
    public function setData($data)
    {
        $this->data = serialize($data);

        return $this;
    }
}