<?php

namespace UTR\Logger;

use PDO;

class Model
{
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * @var \PDOStatement
     */
    private $stmtInsert;


    /**
     * @var \PDOStatement
     */
    private $stmtInsertSearchLog;

    /**
     * @var \PDOStatement
     */
    private $stmtSearchLog;

    public function __construct()
    {
        $this->initDB();
        $this->stmtInsert = $this->pdo->prepare("INSERT INTO `logs` SET
                                  `username` = :username,
                                  `date` = :date,
                                  `action` = :action,
                                  `data` = :data
                              ");
        $this->stmtInsertSearchLog = $this->pdo->prepare("INSERT INTO `search_logs` SET
                                  `client` = :client,
                                  `date` = :date,
                                  `article` = :article,
                                  `brand` = :brand,
                                  `status` = :status,
                                  `article_inside` = :article_inside
                              ");
        $this->stmtSearchLog = $this->pdo->prepare("SELECT * FROM  `search_logs` LIMIT 100");

    }

    public function save(Log $log)
    {
        $params = [
            ':username' => $log->getUsername(),
            ':action' => $log->getAction(),
            ':date' => $log->getDate()->format('Y-m-d H:i:s'),
            ':data' => $log->getSerializedData(),
        ];

        return $this->stmtInsert->execute($params);
    }

    public function getSearchLog()
    {
        $this->stmtSearchLog->execute();

        return $this->stmtSearchLog->fetchAll(PDO::FETCH_ASSOC);
    }

    private function initDB()
    {
        $dsn = 'mysql:host='.MYSQL_SERVER.';dbname='.MYSQL_DATABASE;
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );

        $this->pdo = new PDO($dsn, MYSQL_USER, MYSQL_PASSWORD, $options);
    }

    /**
     * @param SearchLog $log
     * @return array
     */
    public function saveSearchLog(SearchLog $log)
    {
        $params = [
            ':client' => $log->getClient(),
            ':date' => $log->getDate()->format('Y-m-d H:i:s'),
            ':article' => $log->getArticle(),
            ':brand' => $log->getBrand(),
            ':status' => $log->getStatus(),
            ':article_inside' => $log->getArticleInside(),
        ];

        return $this->stmtInsertSearchLog->execute($params);
    }

}