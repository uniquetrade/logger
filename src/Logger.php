<?php

namespace UTR\Logger;

class Logger
{
    const CONFIG_FILE = '/../config/config.php';
    const JOOMLA_CONFIG_FILE = '/config_log.php';

    const ORDER_LOG = 'create_order';
    const FAST_ORDER_LOG = 'fast_order';
    const RESERVE_LOG = 'reserve_order';
    const HISTORY_ORDER_LOG = 'history_order';
    const VIEW_ORDER_LOG = 'view_order';
    const EXPORT_ORDER_LOG = 'export_order';
    const PRINT_ORDER_LOG = 'print_order';
    const SEARCH_OEM_LOG = 'search_by_oem';
    const SEARCH_NAME_LOG = 'search_by_name';
    const SEARCH_OEM_NOT_FOUND_LOG = 'search_by_oem_not_found';
    const SEARCH_NAME_NOT_FOUND_LOG = 'search_by_name_not_found';
    const DOWNLOAD_PRICELIST_LOG = 'pricelist';
    const GENERATE_PRICELIST_LOG = 'pricelist_generate';
    const ADD_TO_CART_LOG = 'add_to_cart';
    const VIEW_REPORT_LOG = 'view_report';
    const LOGIN_LOG = 'login';
    const LOGIN_FAIL_LOG = 'login_fail';

    public static function log($username, $logger, $data)
    {
        try {
            if (self::loadConfig()) {
                $model = new Model();
                $log = new Log($username, $logger, $data);
                $model->save($log);

                if(in_array($logger, [self::SEARCH_OEM_LOG, self::SEARCH_OEM_NOT_FOUND_LOG])) {
                    $searchLog = new SearchLog($logger, $data);
                    $model->saveSearchLog($searchLog);
                }
            }
        } catch (\Exception $e) {

        }

    }

    /**
     * @return bool
     */
    private static function loadConfig()
    {
        $configFileJoomla = JPATH_ROOT . self::JOOMLA_CONFIG_FILE;
        $configFileApp = __DIR__ . self::CONFIG_FILE;

        if (file_exists($configFileJoomla)) {
            include_once $configFileJoomla;
            return true;
        } elseif (file_exists($configFileApp)) {
            include_once $configFileApp;
            return true;
        }

        return false;
    }
}
