CREATE TABLE IF NOT EXISTS `logs` (
  `id` INT UNSIGNED AUTO_INCREMENT NOT NULL,
  `username` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `action` varchar(25) NOT NULL,
  `data` text NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `search_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client` varchar(5) NOT NULL,
  `date` datetime NOT NULL,
  `article` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `status` enum('found','found_available_in_other_city','found_not_available','not_found') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;